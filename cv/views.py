from django.shortcuts import render
from .models import Contact

# Create your views here.
def index(request):
	return render(request, 'cv/index.html')

def project(request):
	return render(request, 'cv/project.html')

def contact(request):
	if request.method == 'POST':
		name = request.POST.get('name')
		email = request.POST.get('email')
		subject = request.POST.get('subject')
		message = request.POST.get('message')

		print(name)

		c = Contact(name=name, email=email, subject=subject, message=message)
		c.save()

		return render(request, 'cv/contact.html')
	else:
		return render(request, 'cv/contact.html')