from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='home'),
	path('projects', views.project, name='project'),
	path('contact', views.contact, name='contact'),
]
